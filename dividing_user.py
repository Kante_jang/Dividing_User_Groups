import pymysql
import pandas as pd
import time
from pandas import Series, DataFrame
import configparser
import datetime, time
import numpy as np
import csv

config = configparser.ConfigParser()
config.read('config.ini')

# stage = 'DEFAULT'
stage = 'REAL'


password = config[stage]['SECRET_KEY'] # 'secret-key-of-myapp'
host_name = config[stage]['AWS_DEFAULT_REGION']
username = config[stage]['ADMIN_NAME']
database_name = config[stage]['DB_NAME']


db = pymysql.connect(
    host=host_name,  # DATABASE_HOST
    port=3306,
    user=username,  # DATABASE_USERNAME
    passwd=password,  # DATABASE_PASSWORD
    db=database_name,  # DATABASE_NAME
    charset='utf8'
)


curs = db.cursor()

sql = """select users.id as user_id, height, weight, bmi, 
          case when bif.bmi<20 then 'A' when 20<=bif.bmi and bif.bmi<24 then 'B' when 24<=bif.bmi and bif.bmi<28 then 'C' ELSE 'D' END as BMI_Group,
          (year(now())-users.birth_year+1) as Age,
          styledb.gstyle
          from users 
          join body_infos as bif on users.id = bif.user_id 
          join (select user_id, group_concat(styles.name) as gstyle from user_styles 
          join styles on user_styles.style_id = styles.id group by user_id) as styledb on styledb.user_id = users.id"""
curs.execute(sql)
df1 = pd.read_sql(sql, db)


# def bmi_height_grouping(strings):
#     # 함수 시간체크-> 첫 번째 실행결과
#     # --- 33.77970767021179 seconds ---
#     # --- 54.445866107940674 seconds ---
#     # --- 52.74603772163391 seconds ---
#     # --- 62.40621995925903 seconds ---
#     print("start function bmi_height_grouping()")
#     start_time = time.time()
#     for i in df_BMI_group[strings].index:
#         if df_BMI_group[strings]["height"][i]<=150:
#             k=1
#             globals()['df' + strings + '{}'.format(k)] = globals()['df' + strings + '{}'.format(k)].append(df_BMI_group[strings].loc[i])
#         elif 151<=df_BMI_group[strings]["height"][i]<155:
#             k=2
#             globals()['df' + strings + '{}'.format(k)] = globals()['df' + strings + '{}'.format(k)].append( df_BMI_group[strings].loc[i])
#         elif 155<=df_BMI_group[strings]["height"][i]<159:
#             k=3
#             globals()['df' + strings + '{}'.format(k)] = globals()['df' + strings + '{}'.format(k)].append(df_BMI_group[strings].loc[i])
#         elif 159<=df_BMI_group[strings]["height"][i]<162:
#             k=4
#             globals()['df' + strings + '{}'.format(k)] = globals()['df' + strings + '{}'.format(k)].append(df_BMI_group[strings].loc[i])
#         elif 162<=df_BMI_group[strings]["height"][i]<165:
#             k=5
#             globals()['df' + strings + '{}'.format(k)] = globals()['df' + strings + '{}'.format(k)].append(df_BMI_group[strings].loc[i])
#         elif 165<=df_BMI_group[strings]["height"][i]<168:
#             k=6
#             globals()['df' + strings + '{}'.format(k)] = globals()['df' + strings + '{}'.format(k)].append(df_BMI_group[strings].loc[i])
#         elif 168<=df_BMI_group[strings]["height"][i]<171:
#             k=7
#             globals()['df' + strings + '{}'.format(k)] = globals()['df' + strings + '{}'.format(k)].append(df_BMI_group[strings].loc[i])
#         elif 171<=df_BMI_group[strings]["height"][i]<174:
#             k=8
#             globals()['df' + strings + '{}'.format(k)] = globals()['df' + strings + '{}'.format(k)].append(df_BMI_group[strings].loc[i])
#         elif 174<=df_BMI_group[strings]["height"][i]<200:
#             k=9
#             globals()['df'+strings+'{}'.format(k)] = globals()['df'+strings+'{}'.format(k)] .append(df_BMI_group[strings].loc[i])
#     print("--- %s seconds ---" % (time.time() - start_time))





# def age_grouping():



# 빈 result DataFrame생성 
result = pd.DataFrame(columns=('id', 'height', 'weight', 'bmi', 'BMI_Group', 'Age'))

# BMI 그룹별로 나누기
df_BMI_group = dict(list(df1.groupby("BMI_Group")))


# 36개 DataFrame 생성 ->bmi, height별
for i in range(1, 10):
    globals()['df' + "A" + '{}'.format(i)] = result
for i in range(1, 10):
    globals()['df' + "B" + '{}'.format(i)] = result
for i in range(1, 10):
    globals()['df' + "C" + '{}'.format(i)] = result
for i in range(1, 10):
    globals()['df' + "D" + '{}'.format(i)] = result


# 180개 DataFrame 생성 ->bmi, height, age별
for i in range(1, 10):
    for j in range(1, 6):
        globals()['df' + "A" + '{}'.format(i)+'{}'.format(j)] = result
for i in range(1, 10):
    for j in range(1, 6):
        globals()['df' + "B" + '{}'.format(i)+'{}'.format(j)] = result
for i in range(1, 10):
    for j in range(1, 6):
        globals()['df' + "C" + '{}'.format(i)+'{}'.format(j)] = result
for i in range(1, 10):
    for j in range(1, 6):
        globals()['df' + "D" + '{}'.format(i)+'{}'.format(j)] = result

# BMI 그룹과 height로 그룹화
# bmi_height_grouping("A")
# bmi_height_grouping("B")
# bmi_height_grouping("C")
# bmi_height_grouping("D")


def easy_bmi_height_grouping(str, groupped_df):
    i = 1
    for key, item in groupped_df:
        globals()['df' + str+ '{}'.format(i)] = groupped_df.get_group(key)
        i += 1


# 키 구간에 따라 그룹화 & BMI 그룹과 height 로 그룹화
resultsA=df_BMI_group["A"].groupby(pd.cut(df_BMI_group["A"]["height"], np.array([0, 150, 154, 158, 161, 164, 167, 170, 173, 200])))
easy_bmi_height_grouping('A', resultsA)
i=1
for key, item in resultsA:
    globals()['dfA_Group'+ '{}'.format(i)] = resultsA.get_group(key).groupby(pd.cut(resultsA.get_group(key)["Age"], np.array([0,19,23,26,29,100])))
    # print(i)
    i+=1


resultsB=df_BMI_group["B"].groupby(pd.cut(df_BMI_group["B"]["height"], np.array([0, 150, 154, 158, 161, 164, 167, 170, 173, 200])))
easy_bmi_height_grouping('B', resultsB)
i=1
for key, item in resultsB:
    globals()['dfB_Group'+ '{}'.format(i)] = resultsB.get_group(key).groupby(pd.cut(resultsB.get_group(key)["Age"], np.array([0,19,23,26,29,100])))
    i+=1


resultsC=df_BMI_group["C"].groupby(pd.cut(df_BMI_group["C"]["height"], np.array([0, 150, 154, 158, 161, 164, 167, 170, 173, 200])))
easy_bmi_height_grouping('C', resultsC)
i=1
for key, item in resultsC:
    globals()['dfC_Group'+ '{}'.format(i)] = resultsC.get_group(key).groupby(pd.cut(resultsC.get_group(key)["Age"], np.array([0,19,23,26,29,100])))
    i+=1


resultsD=df_BMI_group["D"].groupby(pd.cut(df_BMI_group["D"]["height"], np.array([0, 150, 154, 158, 161, 164, 167, 170, 173, 200])))
easy_bmi_height_grouping('D', resultsD)
i=1
for key, item in resultsD:
    globals()['dfD_Group'+ '{}'.format(i)] = resultsD.get_group(key).groupby(pd.cut(resultsD.get_group(key)["Age"], np.array([0,19,23,26,29,100])))
    i+=1

Agearr = ['Y','U','K','M','L']
Heightarr = ['00','01','02','03','04','05','06','07','08']
Bmiarr = ['S','A','C','O']

# a=0
# BMI와 height, Age로 나뉘어진 그룹을 dfA11 부터 dfD910까지 저장 총 180개
k=1
for i in Heightarr:
    j=0

    for key, item in globals()['dfA_Group'+ '{}'.format(k)]:
        globals()[Agearr[j] + "S" +i] = globals()['dfA_Group'+ '{}'.format(k)].get_group(key)
        # print(len(globals()['dfA_Group'+ '{}'.format(i)].get_group(key)))
        # a+=len(globals()['dfA_Group'+ '{}'.format(i)].get_group(key))
        j+=1
    k += 1

k=1
for i in Heightarr:
    j=0
    for key, item in globals()['dfB_Group'+ '{}'.format(k)]:
        globals()[Agearr[j] + "A" + i] = globals()['dfB_Group'+ '{}'.format(k)].get_group(key)
        j+=1
    k += 1

k=1
for i in Heightarr:
    j=0
    for key, item in globals()['dfC_Group'+ '{}'.format(k)]:
        globals()[Agearr[j] + "C" + i] = globals()['dfC_Group'+ '{}'.format(k)].get_group(key)
        j+=1
    k += 1

k=1
for i in Heightarr:
    j=0
    for key, item in globals()['dfD_Group'+ '{}'.format(k)]:
        globals()[Agearr[j] + "O" + i] = globals()['dfD_Group' + '{}'.format(k)].get_group(key)
        j += 1
    k += 1



# for i in range(1,10):
#     for j in range(1,11):
#         print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
#         print(str(i)+" and "+str(j))
#         print(globals()['df' + "A" + '{}'.format(i)+'{}'.format(j)])

# for i in range(1, 10):
#     for j in range(1, 11):
#         globals()['df' + "B" + '{}'.format(i)+'{}'.format(j)] = result
# for i in range(1, 10):
#     for j in range(1, 11):
#         globals()['df' + "B" + '{}'.format(i)+'{}'.format(j)] = result
# for i in range(1, 10):
#     for j in range(1, 11):
#         globals()['df' + "B" + '{}'.format(i)+'{}'.format(j)] = result

# Agearr = ['Y','U','K','M','L']
# Bmiarr = ['S','A','C','O']
# Heightarr = ['00','01','02','03','04','05','06','07','08']
Stylearr = ["SB","CL","ST","LO","FE","MC","UQ","VT","YS","SG","FM","LX","ET","SP"]
Styles=["심플베이직","캐쥬얼","스트릿","러블리","페미닌","모던시크","유니크","빈티지","유니섹스","섹시글램","포멀","럭셔리","에스닉","스포티"]


for i in Agearr:
    for j in Bmiarr:
        for k in Heightarr:
            val = 0
            for l in Stylearr:
                globals()[i + j + k + "_" + l] = globals()[i + j + k][globals()[i+j+k]['gstyle'].str.contains(Styles[val])]
                val+=1


# sql1 = """  select pst.name, count(pst.name) from post_hits
#             left join (select post_id, style_id, st.name from post_styles join styles as st on st.id= post_styles.style_id) as pst on pst.post_id = post_hits.post_id
#             where user_id = 93747
#             group by pst.name"""
# curs.execute(sql1)
# df_ss = pd.read_sql(sql1, db)
#
# sql2 = """  select pst.name, count(pst.name) from post_hits
#             left join (select post_id, style_id, st.name from post_styles join styles as st on st.id= post_styles.style_id) as pst on pst.post_id = post_hits.post_id
#             where user_id = 83935
#             group by pst.name"""
# curs.execute(sql2)
# df_as = pd.read_sql(sql2, db)
# df_result = pd.concat([df_ss,df_as])
# df_result=df_result.groupby(['name']).sum()

import time
start_time1 = time.time()



for i in Agearr:
    print(i)
    for j in Bmiarr:
        for k in Heightarr:
            for l in Stylearr:
                df_style = pd.DataFrame({},columns=['name','count(pst.name)'])
                start_time = time.time()
                userarr = ""
                for userid in  globals()[i + j + k + "_" + l]["user_id"]:
                    userarr = userarr + str(userid) + ","
                if not userarr:
                    print(i,j,k,l+"is Empty table")
                    userarr = userarr + str(0)
                userarr = "("+userarr+")"
                userarr = userarr.replace(",)", ")")
                # print(userarr)
                sql = " select pst.name, count(pst.name) from post_hits left join (select post_id, style_id, st.name from post_styles join styles as st on st.id= post_styles.style_id) as pst on pst.post_id = post_hits.post_id where user_id in "+userarr+" group by pst.name"
                curs.execute(sql)
                df_ex = pd.read_sql(sql,db)
                df_style = pd.concat([df_style, df_ex])
                print("--- %s seconds ---" % (time.time() - start_time))

                # print(df_style)
                # print(l)
                globals()[i + j + k + "_" + l+"_style"]=df_style.groupby(['name']).sum()
                globals()[i + j + k + "_" + l + "_style"]= globals()[i + j + k + "_" + l + "_style"].sort_values(by=['count(pst.name)'],axis=0,ascending=False)
                globals()[i + j + k + "_" + l + "_style"] = globals()[i + j + k + "_" + l+"_style"].reset_index()
                # print(globals()[i + j + k + "_" + l+"_style"])
                # print(pd.concat([globals()[i + j + k + "_" + l+"_style"], globals()[i + j + k + "_" + l]], axis=1))
                # hgkfghk

for i in Agearr:
    for j in Bmiarr:
        for k in Heightarr:
            for l in Stylearr:
                df_style = pd.DataFrame(columns=('title', 'count(tgs.title)'))
                start_time = time.time()
                userarr = ""
                for userid in globals()[i + j + k + "_" + l]["user_id"]:
                    userarr = userarr + str(userid) + ","
                # print(userarr)
                if not userarr:
                    print(i, j, k, l + "is Empty table")
                    userarr = userarr + str(0)
                userarr = "(" + userarr + ")"
                userarr = userarr.replace(",)", ")")

                sql = " select tgs.title, count(tgs.title) from post_hits left join (select post_id, tag_id, title from taggings join tags as tg on tg.id = taggings.tag_id) as tgs on tgs.post_id = post_hits.post_id where user_id in" +userarr + " group by tgs.title"
                curs.execute(sql)
                df_ex = pd.read_sql(sql, db)
                df_style = pd.concat([df_style, df_ex])
                print("--- %s seconds ---" % (time.time() - start_time))
                    # print(df_style)
                globals()[i + j + k + "_" + l + "_tags"] = df_style.groupby(['title']).sum()
                globals()[i + j + k + "_" + l + "_tags"] = globals()[i + j + k + "_" + l + "_tags"].sort_values(by=['count(tgs.title)'], axis=0, ascending=False)
                globals()[i + j + k + "_" + l + "_tags"] = globals()[i + j + k + "_" + l + "_tags"].reset_index()
            # YS00_SB_style.to_csv("output.csv", sep=',', encoding='utf-8')

print("--- %s seconds ---" % (time.time() - start_time1))

for i in Agearr:
    for j in Bmiarr:
        df_csv_final = pd.DataFrame()
        for k in Heightarr:
            df_csv_result = pd.DataFrame()
            for l in Stylearr:
                # 스타일태그, 포스트태그 단순 합치기
                df_csv = pd.concat([globals()[i + j + k + "_" + l+"_style"], globals()[i + j + k + "_" + l + "_tags"]], axis=1)
                # 나이,BMI,키 그룹에 스타일로 그룹화 한 DataFrame에 이름 붙이기
                df_csv = pd.concat({i+j+k+"_"+l : pd.DataFrame(df_csv)},axis=1)
                # df_csv를 다 합친 것
                df_csv_result = pd.concat([df_csv_result, df_csv],axis=1)

            df_csv_final = pd.concat([df_csv_final, df_csv_result], axis=1)
        # writer = pd.ExcelWriter(i+j+".xlsx", engine='xlsxwriter', options={'strings_to_urls': False})
        # df_csv_final.to_excel(writer)
        df_csv_final.to_csv(i+j+".csv", sep=',', encoding='utf-8-sig')
        # df_csv_final.to_excel(i+j+".xlsx",sheet_name='sheet1')

            # print(str(globals()[i + j + k + "_" + l+"_results"]))
            # if globals()[i + j + k + "_" + l+"_results"] == 'YS03_VT_results':
            #         print(YS03_VT_results)





# print(YS04_SB)
# print(YS04_SB["id"])
# for index in YS04_SB["user_id"]:
#     print(index)

# print(UA03_SB_results)
        # print(df1.loc[i])
# df1.groupby(50<["weight"])
# means = means[strings].groupby(pd.cut(means[strings]["height"], np.arange(150,200,4)))
print("@@@@@@@@@@@@@@@@@@@@@@@@@@@")
# print(a)
# print(dfA11[dfA11['gstyle'].str.contains("러블리")])
# print(UC04)

# for i in range









# print(dfA1)

# for key, item in qpqpqp:
#     print(key)
#     print(qpqpqp.get_group(key), "\n\n")


# val=0
# for i in range(1, 10):
#     val+= len(globals()['df' + "A" + '{}'.format(i)])
# for i in range(1, 10):
#     val+= len(globals()['df' + 'B' + '{}'.format(i)])
# for i in range(1, 10):
#     val+= len(globals()['df' + "C" + '{}'.format(i)])
# for i in range(1, 10):
#     val+= len(globals()['df' + "D" + '{}'.format(i)])

# print(np.arange(5.0, -0.5, -0.5))
# print(results.head(88))
# df_result.to_csv("output.csv", sep=',', encoding='utf-8')
# a= 0
# df_result = result
# for strn in range(65, 69):
#     print(chr(strn))
#     for i in range(1, 10):
#         print("i = "+str(i))
#         for j in range(1, 6):
#             a+=len(globals()['df' + chr(strn) + '{}'.format(i)+'{}'.format(j)])
#             df_result=pd.concat([df_result, globals()['df' + chr(strn) + '{}'.format(i)+'{}'.format(j)]])
# print(a)
# print (dfA11)
# print (dfA12)
# print (dfA13)
# print (dfA14)
# print (dfA15)
# df_result.to_csv("output.csv", sep=',', encoding='utf-8')
db.close()
import pymysql
import pandas as pd
import time
from pandas import Series, DataFrame
import configparser
import datetime, time
import numpy as np
import csv

config = configparser.ConfigParser()
config.read('config.ini')

# stage = 'DEFAULT'
stage = 'REAL'


password = config[stage]['SECRET_KEY'] # 'secret-key-of-myapp'
host_name = config[stage]['AWS_DEFAULT_REGION']
username = config[stage]['ADMIN_NAME']
database_name = config[stage]['DB_NAME']


db = pymysql.connect(
    host=host_name,  # DATABASE_HOST
    port=3306,
    user=username,  # DATABASE_USERNAME
    passwd=password,  # DATABASE_PASSWORD
    db=database_name,  # DATABASE_NAME
    charset='utf8'
)


# week1 = len(df_active_logs[(df_active_logs.created_at > minus_time(st2, 8)) & (df_active_logs.created_at < minus_time(st2, 2)) & (df_active_logs.active_date == st2.date())])
def get_var_name(**kwargs): return kwargs.keys()[0]


curs = db.cursor()

sql = "select user_id, pst.name from post_hits left join (select post_id, style_id, st.name from post_styles join styles as st on st.id= post_styles.style_id) as pst on pst.post_id = post_hits.post_id limit 50000"
curs.execute(sql)
df_styles = pd.read_sql(sql,db)

k = (1,31,37)
for i in range(1,3):
    globals()[str(i)+"Good"] = []
for i in range(1,3):
    print(globals()[str(i)+"Good"].keys[0])

df_styles = df_styles[df_styles['user_id'].isin(k)]
# print(df_styles)
# for i in range(1,3):
#     str_ex = str(globals()[str(i)+"Good"])
#     df_styles.to_csv(str_ex+".csv", sep=',', encoding='utf-8')
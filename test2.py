import pymysql
import pandas as pd
import time
from pandas import Series, DataFrame
import configparser
import datetime, time
import numpy as np
import csv

config = configparser.ConfigParser()
config.read('config.ini')

# stage = 'DEFAULT'
stage = 'REAL'


password = config[stage]['SECRET_KEY'] # 'secret-key-of-myapp'
host_name = config[stage]['AWS_DEFAULT_REGION']
username = config[stage]['ADMIN_NAME']
database_name = config[stage]['DB_NAME']


db = pymysql.connect(
    host=host_name,  # DATABASE_HOST
    port=3306,
    user=username,  # DATABASE_USERNAME
    passwd=password,  # DATABASE_PASSWORD
    db=database_name,  # DATABASE_NAME
    charset='utf8'
)


curs = db.cursor()
df1 = pd.DataFrame({
    '고객번호': [1001, 1002, 1003, 1004, 1005, 1006, 1007],
    '이름': ['둘리', '도우너', '또치', '길동', '희동', '마이콜', '영희']
}, columns=['고객번호', '이름'])
df = pd.DataFrame()
for i in range(1,3):
    df = pd.concat([df, df1], axis = 1)
df = pd.concat({"Jiseok": pd.DataFrame(df)}, axis=1)

print (df)


db.close()